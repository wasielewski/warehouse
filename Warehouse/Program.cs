﻿namespace Warehouse
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var warehouseAnalyzer = new RegisterContainers().GetInitialServiceService();

            warehouseAnalyzer.Start();
        }
    }
}