﻿namespace Common.Models
{
    public class ItemDetails
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}