﻿using System.Collections.Generic;

namespace Common.Models
{
    public class WarehouseDetails
    {
        public int TotalQuantity { get; set; }
        public string WarehouseName { get; set; }
        public List<ItemDetails> Items { get; set; }
    }
}